package com.cometproject.server.storage.queries.items;

import com.cometproject.server.game.catalog.purchase.CatalogPurchase;
import com.cometproject.server.game.catalog.purchase.OldCatalogPurchaseHandler;
import com.cometproject.server.game.items.ItemManager;
import com.cometproject.server.game.items.types.ItemDefinition;
import com.cometproject.server.storage.SqlHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ItemDao {
    public static Map<Integer, ItemDefinition> getDefinitions() {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        Map<Integer, ItemDefinition> data = new HashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("SELECT * FROM furniture", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
//                if(resultSet.getString("id").length() > 9) continue;
                try {
                    data.put(resultSet.getInt("id"), new ItemDefinition(resultSet));
                } catch (Exception e) {
                    ItemManager.getInstance().getLogger().warn("Error while loading item definition for ID: " + resultSet.getInt("id"), e);
                }
            }

        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }

    public static long createItem(int userId, int itemId, String data) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("INSERT into items (`user_id`, `room_id`, `base_item`, `extra_data`, `x`, `y`, `z`, `rot`, `wall_pos`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", sqlConnection, true);

            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, 0);
            preparedStatement.setInt(3, itemId);
            preparedStatement.setString(4, data);
            preparedStatement.setInt(5, 0);
            preparedStatement.setInt(6, 0);
            preparedStatement.setInt(7, 0);
            preparedStatement.setInt(8, 0);
            preparedStatement.setString(9, "");

            SqlHelper.executeStatementSilently(preparedStatement, false);

            resultSet = preparedStatement.getGeneratedKeys();

            while (resultSet.next()) {
                return resultSet.getLong(1);
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return 0;
    }

    public static List<Long> createItems(List<CatalogPurchase> catalogPurchases) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<Long> data = new ArrayList<>();

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("INSERT into items (`user_id`, `room_id`, `base_item`, `extra_data`, `x`, `y`, `z`, `rot`, `wall_pos`) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);", sqlConnection, true);

            for (CatalogPurchase purchase : catalogPurchases) {
                preparedStatement.setInt(1, purchase.getPlayerId());
                preparedStatement.setInt(2, 0);
                preparedStatement.setInt(3, purchase.getItemBaseId());
                preparedStatement.setString(4, purchase.getData());
                preparedStatement.setInt(5, 0);
                preparedStatement.setInt(6, 0);
                preparedStatement.setInt(7, 0);
                preparedStatement.setInt(8, 0);
                preparedStatement.setString(9, "");

                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

            resultSet = preparedStatement.getGeneratedKeys();

            while (resultSet.next()) {
                data.add(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }

    public static boolean updateSetItem(long itemId, String type, String value) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        switch (type)
        {
            case "width":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET width = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "length":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET length = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "height":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET stack_height = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setDouble(1, Double.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "stack":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET can_stack = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "walk":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET is_walkable = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "sit":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET can_sit = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "states":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET interaction_modes_count = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "interaction":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET interaction_type = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setString(1, String.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "vending":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET vending_ids = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setString(1, String.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "multiheight":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET variable_heights = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setString(1, String.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            case "effect":
                try {
                    sqlConnection = SqlHelper.getConnection();

                    preparedStatement = SqlHelper.prepare("UPDATE furniture SET effect_id = ? WHERE id = ? LIMIT 1", sqlConnection, true);

                    preparedStatement.setInt(1, Integer.valueOf(value));
                    preparedStatement.setLong(2, itemId);

                    SqlHelper.executeStatementSilently(preparedStatement, false);

                    resultSet = preparedStatement.getGeneratedKeys();
                } catch (SQLException e) {
                    SqlHelper.handleSqlException(e);
                } finally {
                    SqlHelper.closeSilently(resultSet);
                    SqlHelper.closeSilently(preparedStatement);
                    SqlHelper.closeSilently(sqlConnection);
                }
                break;
            default:
                return false;
        }

        return true;
    }

}
