package com.cometproject.server.network.messages.outgoing.user.camera;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;
import com.cometproject.server.protocol.headers.Composers;


public class CameraRoomThumbnailSavedComposer extends MessageComposer {

    public CameraRoomThumbnailSavedComposer() {}

    @Override
    public short getId() {
        return Composers.CameraRoomThumbnailSavedMessageComposer;
    }

    @Override
    public void compose(IComposer msg) {}
}
