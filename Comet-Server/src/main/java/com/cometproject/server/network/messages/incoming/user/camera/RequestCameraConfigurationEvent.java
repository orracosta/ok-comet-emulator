package com.cometproject.server.network.messages.incoming.user.camera;

import com.cometproject.server.network.messages.incoming.Event;
import com.cometproject.server.network.messages.outgoing.user.camera.CameraPriceMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.camera.CameraURLMessageComposer;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.protocol.messages.MessageEvent;


public class RequestCameraConfigurationEvent implements Event {
    @Override
    public void handle(Session client, MessageEvent msg) throws Exception {
        client.send(new CameraPriceMessageComposer(500,0,5));
    }
}