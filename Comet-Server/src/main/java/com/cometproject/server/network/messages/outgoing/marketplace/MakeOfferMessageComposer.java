package com.cometproject.server.network.messages.outgoing.marketplace;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;

public class MakeOfferMessageComposer extends MessageComposer {
    private final int code;

    public MakeOfferMessageComposer(int code) {
        this.code = code;
    }

    public short getId() {
        return 3982;
    }

    public void compose(IComposer msg) {
        msg.writeInt(this.code);
    }
}
