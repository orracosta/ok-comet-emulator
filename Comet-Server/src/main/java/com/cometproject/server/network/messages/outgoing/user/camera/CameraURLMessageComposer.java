package com.cometproject.server.network.messages.outgoing.user.camera;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;
import com.cometproject.server.protocol.headers.Composers;


public class CameraURLMessageComposer extends MessageComposer {
    private final String URL;

    public CameraURLMessageComposer(String url) {
        URL = url;
    }

    @Override
    public short getId() {
        return Composers.CameraURLMessageComposer;
    }

    @Override
    public void compose(IComposer msg) {
        msg.writeString(this.URL);
    }
}
