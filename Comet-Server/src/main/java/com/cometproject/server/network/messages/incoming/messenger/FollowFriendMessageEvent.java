package com.cometproject.server.network.messages.incoming.messenger;

import com.cometproject.api.game.rooms.settings.RoomAccessType;
import com.cometproject.server.game.groups.GroupManager;
import com.cometproject.server.game.groups.types.Group;
import com.cometproject.server.game.players.components.types.messenger.MessengerFriend;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.network.messages.incoming.Event;
import com.cometproject.server.network.messages.outgoing.room.engine.RoomForwardMessageComposer;
import com.cometproject.server.protocol.messages.MessageEvent;
import com.cometproject.server.network.sessions.Session;


public class FollowFriendMessageEvent implements Event {
    public void handle(Session client, MessageEvent msg) {
        int friendId = msg.readInt();

        MessengerFriend friend = client.getPlayer().getMessenger().getFriendById(friendId);

        if (friend == null || !friend.isInRoom())
            return;

        Room room = friend.getSession().getPlayer().getEntity().getRoom();

        if (room == null) {
            // wtf?
            return;
        }

        if (client.getPlayer().getEntity() != null && client.getPlayer().getEntity().getRoom() != null) {
            Room roomInstance = client.getPlayer().getEntity().getRoom();

            if (roomInstance.getId() == room.getId()) {
                //client.getPlayer().getEntity().leaveRoom(false, false, false);
                return;
            }

            if (room.getData().getAccess() == RoomAccessType.INVISIBLE && client.getPlayer().getData().getRank() < 3) {
                final Group group = GroupManager.getInstance().getGroupByRoomId(room.getId());

                if (room.getGroup() != null) {
                    if (!client.getPlayer().getGroups().contains(group.getId())) {
                        client.getPlayer().getEntity().leaveRoom(false, false, true);
                        return;
                    }
                } else {
                    if (!room.getRights().hasRights(client.getPlayer().getId())) {
                        client.getPlayer().getEntity().leaveRoom(false, false, true);
                        return;
                    }
                }
            }
        }

        client.send(new RoomForwardMessageComposer(room.getId()));
    }
}