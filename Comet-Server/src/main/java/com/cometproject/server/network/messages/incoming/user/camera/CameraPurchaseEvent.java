package com.cometproject.server.network.messages.incoming.user.camera;

import com.cometproject.api.game.players.data.components.inventory.PlayerItem;
import com.cometproject.server.config.CometSettings;
import com.cometproject.server.game.achievements.types.AchievementType;
import com.cometproject.server.game.players.components.types.inventory.InventoryItem;
import com.cometproject.server.network.messages.incoming.Event;
import com.cometproject.server.network.messages.outgoing.catalog.UnseenItemsMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.camera.CameraPurchaseSuccesfullComposer;
import com.cometproject.server.network.messages.outgoing.user.inventory.UpdateInventoryMessageComposer;
import com.cometproject.server.protocol.messages.MessageEvent;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.items.ItemDao;
import com.google.common.collect.Sets;


public class CameraPurchaseEvent implements Event {
    @Override
    public void handle(Session client, MessageEvent msg) throws Exception {

        if (client.getPlayer().getLastPhotoTaken() != 0) {
            long itemId = ItemDao.createItem(client.getPlayer().getId(), CometSettings.cameraPhotoItemId, client.getPlayer().getPhotoJSON());
            final PlayerItem playerItem = new InventoryItem(itemId, CometSettings.cameraPhotoItemId, client.getPlayer().getPhotoJSON());

            client.getPlayer().getInventory().addItem(playerItem);

            client.send(new CameraPurchaseSuccesfullComposer());
            client.send(new UpdateInventoryMessageComposer());
            client.send(new UnseenItemsMessageComposer(Sets.newHashSet(playerItem)));

            //client.getPlayer().getAchievements().progressAchievement(AchievementType.CAMERA_PHOTO, 1);
        }
    }
}