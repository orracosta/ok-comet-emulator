package com.cometproject.server.network.messages.outgoing.room.freeze;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;
import com.cometproject.server.protocol.headers.Composers;

public class UpdateFreezeLivesMessageComposer extends MessageComposer {
    private final int playerId;
    private final int freezeLives;

    public UpdateFreezeLivesMessageComposer(int playerId, int freezeLives) {
        this.playerId = playerId;
        this.freezeLives = freezeLives;
    }

    @Override
    public short getId() {
        return Composers.UpdateFreezeLivesMessageComposer;
    }

    @Override
    public void compose(IComposer msg) {
        msg.writeInt(this.playerId);
        msg.writeInt(this.freezeLives);
    }
}
