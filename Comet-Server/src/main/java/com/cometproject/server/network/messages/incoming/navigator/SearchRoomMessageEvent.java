package com.cometproject.server.network.messages.incoming.navigator;

import com.cometproject.api.game.rooms.settings.RoomAccessType;
import com.cometproject.server.game.rooms.RoomManager;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.game.rooms.types.RoomData;
import com.cometproject.server.network.messages.incoming.Event;
import com.cometproject.server.network.messages.outgoing.navigator.NavigatorFlatListMessageComposer;
import com.cometproject.server.protocol.messages.MessageEvent;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.rooms.RoomDao;

import java.util.List;


public class SearchRoomMessageEvent implements Event {
    public void handle(Session client, MessageEvent msg) {
        String query = msg.readString();
        List<RoomData> roomSearchResults = RoomManager.getInstance().getRoomsByQuery(query);
        List<RoomData> toShow = null;

        for (RoomData roomData : roomSearchResults) {
            Room room = RoomManager.getInstance().get(roomData.getId());
            if(roomData.getAccess() == RoomAccessType.INVISIBLE && client.getPlayer().getData().getRank() < 3)
            {
                if(!room.getRights().hasRights(client.getPlayer().getId(), true))
                    continue;
            }
            toShow.add(roomData);
        }

        if(toShow != null) {
            client.send(new NavigatorFlatListMessageComposer(8, "", toShow));
        }
    }
}
