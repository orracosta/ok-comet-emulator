package com.cometproject.server.network.messages.incoming.user.camera;

import com.cometproject.api.game.players.data.components.inventory.PlayerItem;
import com.cometproject.server.config.CometSettings;
import com.cometproject.server.config.Locale;
import com.cometproject.server.game.achievements.types.AchievementType;
import com.cometproject.server.game.players.components.types.inventory.InventoryItem;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.network.messages.incoming.Event;
import com.cometproject.server.network.messages.outgoing.catalog.UnseenItemsMessageComposer;
import com.cometproject.server.network.messages.outgoing.notification.NotificationMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.camera.CameraRoomThumbnailSavedComposer;
import com.cometproject.server.network.messages.outgoing.user.camera.CameraURLMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.inventory.UpdateInventoryMessageComposer;
import com.cometproject.server.protocol.messages.MessageEvent;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.items.ItemDao;
import com.google.common.collect.Sets;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class CameraRoomThumbnailEvent implements Event {
    @Override
    public void handle(Session client, MessageEvent msg) throws Exception {
        Room room = client.getPlayer().getEntity().getRoom();

        if (room == null)
            return;

        if(room.getData().getOwnerId() != client.getPlayer().getId() && !client.getPlayer().getPermissions().getRank().roomFullControl())
            return;

        final int count = msg.readInt();
        ByteBuf image = msg.getBuffer().readBytes(count);

        if(image == null)
            return;

        BufferedImage theImage = ImageIO.read(new ByteBufInputStream(image));
        ImageIO.write(theImage, "png", new File(CometSettings.cameraLocationThumbnail + room.getId() + ".png"));

        client.send(new CameraRoomThumbnailSavedComposer());
    }
}
