package com.cometproject.server.network.messages.outgoing.user.camera;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;
import com.cometproject.server.protocol.headers.Composers;


public class CameraPriceMessageComposer extends MessageComposer {
    public final int credits;
    public final int points;
    public final int pointsType;

    public CameraPriceMessageComposer(int credits, int points, int pointsType) {
        this.credits = credits;
        this.points = points;
        this.pointsType = pointsType;
    }

    @Override
    public short getId() {
        return Composers.CameraPriceMessageComposer;
    }

    @Override
    public void compose(IComposer msg) {
        msg.writeInt(this.credits);
        msg.writeInt(this.points);
        msg.writeInt(this.pointsType);
    }
}
