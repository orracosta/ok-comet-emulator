package com.cometproject.server.network.messages.outgoing.user.details;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;
import com.cometproject.server.protocol.headers.Composers;

public class UpdateUsernameMessageComposer extends MessageComposer {
    private String user;
    private int virtualid;
    private int roomid;

    public UpdateUsernameMessageComposer(String user, int virtualid, int roomid) {
        this.user = user;
        this.roomid = roomid;
        this.virtualid = virtualid;
    }

    @Override
    public short getId() {
        return Composers.UpdateUsernameMessageComposer;
    }

    @Override
    public void compose(IComposer msg) {
        msg.writeInt(0);
        msg.writeString(user);
        msg.writeInt(0);
    }
}
