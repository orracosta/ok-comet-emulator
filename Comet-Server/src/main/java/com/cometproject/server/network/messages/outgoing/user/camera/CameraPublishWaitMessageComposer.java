package com.cometproject.server.network.messages.outgoing.user.camera;

import com.cometproject.api.networking.messages.IComposer;
import com.cometproject.server.network.messages.composers.MessageComposer;
import com.cometproject.server.protocol.headers.Composers;


public class CameraPublishWaitMessageComposer extends MessageComposer {
    public final boolean published;
    public final int seconds;
    public final String unknownString;

    public CameraPublishWaitMessageComposer(boolean published, int seconds, String unknownString) {
        this.published = published;
        this.seconds = seconds;
        this.unknownString = unknownString;
    }

    @Override
    public short getId() {
        return Composers.AchievementScoreMessageComposer;
    }

    @Override
    public void compose(IComposer msg) {
        msg.writeBoolean(this.published);
        msg.writeInt(this.seconds);

        if (this.published)
        {
            msg.writeString(this.unknownString);
        }
    }
}
