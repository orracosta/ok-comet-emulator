package com.cometproject.server.game.commands.user;

import com.cometproject.server.boot.Comet;
import com.cometproject.server.config.CometSettings;
import com.cometproject.server.config.Locale;
import com.cometproject.server.game.GameCycle;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.network.messages.outgoing.notification.AdvancedAlertMessageComposer;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.api.stats.CometStats;

import java.text.NumberFormat;


public class AboutCommand extends ChatCommand {

    @Override
    public void execute(Session client, String message[]) {
        StringBuilder about = new StringBuilder();
        NumberFormat format = NumberFormat.getInstance();

        CometStats cometStats = Comet.getStats();

        boolean aboutDetailed = client.getPlayer().getPermissions().getRank().aboutDetailed();
        boolean aboutStats = client.getPlayer().getPermissions().getRank().aboutStats();

        if (CometSettings.aboutShowRoomsActive || CometSettings.aboutShowRoomsActive || CometSettings.aboutShowUptime || aboutDetailed) {
            about.append("<b>Estatisticas do servidor:</b><br>");

            if (CometSettings.aboutShowPlayersOnline || aboutDetailed)
                about.append("<b>Usuários Online</b>: " + format.format(cometStats.getPlayers()) + "<br>");

            if (CometSettings.aboutShowRoomsActive || aboutDetailed)
                about.append("<b>Quartos ativos</b>: " + format.format(cometStats.getRooms()) + "<br>");

            if (CometSettings.aboutShowUptime || aboutDetailed)
                about.append("<b>Online por</b>: " + cometStats.getUptime() + "<br>");
        }

        if (aboutStats) {
            about.append("<br><br><b>Recorde Online:</b><br>");
            about.append("Recorde Global: " + GameCycle.getInstance().getOnlineRecord() + "<br>");
            about.append("Recorde do último reboot: " + GameCycle.getInstance().getCurrentOnlineRecord() + "<br>");
        }

        about.append("<br><b>- All thanks to Leon (Comet Creator).</b><br>");


        client.send(new AdvancedAlertMessageComposer(
                "Comet Server (Mania Edition) - v: 1.3.0",
                about.toString(),
                "www.mania.gg", "https://www.mania.gg", CometSettings.aboutImg
        ));
    }

    @Override
    public String getPermission() {
        return "about_command";
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.about.description");
    }
}