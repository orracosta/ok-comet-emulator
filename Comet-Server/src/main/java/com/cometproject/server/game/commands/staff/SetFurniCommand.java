package com.cometproject.server.game.commands.staff;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.game.items.ItemManager;
import com.cometproject.server.game.items.types.ItemDefinition;
import com.cometproject.server.game.rooms.objects.entities.pathfinding.types.ItemPathfinder;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.game.rooms.types.components.ItemsComponent;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.items.ItemDao;
import com.cometproject.server.tasks.executors.engine.ItemUpdateEvent;


public class SetFurniCommand extends ChatCommand {

    @Override
    public void execute(Session client, String[] params) {
        if (client.getPlayer().getEntity() != null && client.getPlayer().getEntity().getRoom() != null) {
            if (!client.getPlayer().getEntity().getRoom().getRights().hasRights(client.getPlayer().getId()) &&
                    !client.getPlayer().getPermissions().getRank().roomFullControl()) {
                sendNotif(Locale.getOrDefault("command.need.rights", "You need rights to use this command in this room!"), client);
                return;
            }

            Room room = client.getPlayer().getEntity().getRoom();
            RoomItemFloor item = room.getItems().getFloorItem(room.getMapping().getTile(client.getPlayer().getEntity().getPosition()).getTopItem());

            if (item == null)
            {
                return;
            }

            if (params.length == 1)
            {
                if (params[0].equalsIgnoreCase("info"))
                {
                    String message = "Opções do SET\r\n";
                    message += "Use: :set <item_id> <opcao> <valor>\r\n";
                    message += "width - Número (inteiro)\r";
                    message += "length - Número (inteiro)\r";
                    message += "height - Número (Decimal)\r";
                    message += "stack - 1/0\r";
                    message += "walk - 1/0\r";
                    message += "sit - 1/0\r";
                    message += "states - Número (inteiro)\r";
                    message += "vending - iditem (caso mais bebidas: iditem1,iditem2 (sem espaço com virgula))\r";
                    message += "interaction - InteractionType (Caso não souber, use 'default' sem aspas)\r";
                    message += "multiheight - altura1,altura2,altura3 (sem espaço com virgula)\r";
                    message += "effect - ID do efeito\r";
                    message += "get - Exibe informações do mobi\r";
                    sendAlert(message, client);
                    return;
                }
                else if (params[0].equalsIgnoreCase("get"))
                {
                    String message = "item name: "+ item.getDefinition().getItemName() +"\n";
                    message += "width: "+ item.getDefinition().getWidth() +"\r";
                    message += "length: "+ item.getDefinition().getLength() +"\r";
                    message += "height: "+ item.getDefinition().getHeight() +"\r";
                    message += "stack: "+ item.getDefinition().canStack() +"\r";
                    message += "walk: "+ item.getDefinition().canWalk() +"\r";
                    message += "sit: "+ item.getDefinition().canSit() +"\r";
                    message += "states: "+ item.getDefinition().getInteractionCycleCount() +"\r";
                    message += "interaction: "+ item.getDefinition().getInteraction() +"\r";
                    message += "effects: "+ item.getDefinition().getEffectId()+"\r";
                    sendAlert(message, client);
                    return;
                }
            }
            if (params.length >= 2)
            {
                boolean result = ItemDao.updateSetItem(item.getDefinition().getId(), params[0], params[1]);
                if(result)
                {
                    sendWhisper("Item atualizado com sucesso!", client);
                    ItemManager.getInstance().loadItemDefinitions();
                }
                else
                {
                    sendWhisper("O item não foi atualizado!", client);
                }
            }
        }
    }

    @Override
    public String getPermission() {
        return "setfurni_command";
    }
    
    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.setfurni.description");
    }
}
