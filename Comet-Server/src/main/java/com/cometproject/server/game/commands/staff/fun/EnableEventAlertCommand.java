package com.cometproject.server.game.commands.staff.fun;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.network.sessions.Session;

public class EnableEventAlertCommand extends ChatCommand {
    @Override
    public void execute(Session client, String[] params) {
        if (client == null || client.getPlayer() == null || client.getPlayer().getSettings() == null) {
            return;
        }

        client.getPlayer().getSettings().setEnableEventNotif(true);

        EnableEventAlertCommand.sendNotif(Locale.get("command.enableeventnotif.txt"), client);
    }

    @Override
    public String getPermission() {
        return "enableeventnotif_command";
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.enableeventnotif.description");
    }
}
