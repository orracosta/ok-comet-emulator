package com.cometproject.server.game.rooms.objects.items.types.floor.vip;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.rooms.objects.entities.RoomEntity;
import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFactory;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.types.Room;


public class VipGateFloorItem extends RoomItemFloor {
    public boolean isOpen;

    public VipGateFloorItem(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);

        this.isOpen = false;
        this.setExtraData(0);
    }

    @Override
    public boolean onInteract(RoomEntity entity0, int requestData, boolean isWiredTrigger) {
        return false;
    }

    @Override
    public void onEntityStepOn(RoomEntity entity) {
        this.isOpen = true;
        this.setExtraData(1);
        this.sendUpdate();
    }

    @Override
    public void onEntityStepOff(RoomEntity entity) {
        this.setTicks(RoomItemFactory.getProcessTime(this.getDefinition().getInteractionCycleCount()));
    }

    @Override
    public void onTickComplete() {
        if(this.getTile().getEntities().size() != 0) {
            return;
        }

        this.isOpen = false;
        this.setExtraData(0);
        this.sendUpdate();
    }

    @Override
    public boolean isMovementCancelled(RoomEntity entity) {

        if(!(entity instanceof PlayerEntity)) {
            return true;
        }

        final PlayerEntity playerEntity = (PlayerEntity) entity;

        if(playerEntity.getPlayer().getData().isVip() || playerEntity.getPlayer().getData().getRank() > 3)
        {
            return false;
        }
        playerEntity.getPlayer().sendAdvancedMessage(Locale.get("vip.promo.title"), Locale.get("vip.promo.text"), Locale.get("vip.promo.button"), Locale.get("vip.promo.link"), "vip_info");
        return true;
    }

    public boolean isOpen() {
        return !this.getExtraData().equals("0");
    }
}
