package com.cometproject.server.game.rooms.objects.items.types.floor.wired.triggers;

import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.base.WiredTriggerItem;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.events.WiredItemEvent;
import com.cometproject.server.game.rooms.types.Room;

import java.util.List;


public class WiredTriggerPeriodicallyIdle extends WiredTriggerItem {
    private static final int PARAM_TICK_LENGTH = 0;

    private final WiredItemEvent event;

    /**
     * The default constructor
     *
     * @param id       The ID of the item
     * @param itemId   The ID of the item definition
     * @param room     The instance of the room
     * @param owner    The ID of the owner
     * @param x        The position of the item on the X axis
     * @param y        The position of the item on the Y axis
     * @param z        The position of the item on the z axis
     * @param rotation The orientation of the item
     * @param data     The JSON object associated with this item
     */
    public WiredTriggerPeriodicallyIdle(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);

        this.getWiredData().getParams().putIfAbsent(PARAM_TICK_LENGTH, 5);

        this.event = new WiredItemEvent(null, null);

        event.setTotalTicks(this.getTickCount());
        this.queueEvent(event);
    }

    @Override
    public boolean suppliesPlayer() {
        return true;
    }

    @Override
    public void onEventComplete(WiredItemEvent event) {
        List<PlayerEntity> roomplayers = this.getRoom().getEntities().getPlayerEntities();

        for (PlayerEntity player : roomplayers) {
            if(player == null)
                continue;

            if(player.isIdle())
                this.evaluate(player, null);
        }

        // loop
        this.event.setTotalTicks(this.getTickCount());
        this.queueEvent(this.event);
    }

    @Override
    public void onDataChange() {
        this.event.setTotalTicks(this.getTickCount());
    }

    @Override
    public int getInterface() {
        return 6;
    }

    public int getTickCount() {
        int tickLength = this.getWiredData().getParams().get(PARAM_TICK_LENGTH);

        if(tickLength < 1) {
            tickLength = 1;
        }

        return tickLength;
    }
}
