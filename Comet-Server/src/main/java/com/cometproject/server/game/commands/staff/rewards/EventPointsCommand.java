package com.cometproject.server.game.commands.staff.rewards;

import com.cometproject.api.game.players.data.components.inventory.PlayerItem;
import com.cometproject.server.boot.Comet;
import com.cometproject.server.config.CometSettings;
import com.cometproject.server.config.Locale;
import com.cometproject.server.config.NewConfig;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.game.players.components.types.inventory.InventoryItem;
import com.cometproject.server.game.players.data.PlayerData;
import com.cometproject.server.network.NetworkManager;
import com.cometproject.server.network.messages.outgoing.catalog.UnseenItemsMessageComposer;
import com.cometproject.server.network.messages.outgoing.notification.NotificationMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.inventory.UpdateInventoryMessageComposer;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.items.ItemDao;
import com.cometproject.server.storage.queries.player.PlayerDao;
import com.google.common.collect.Sets;

public class EventPointsCommand extends ChatCommand {
    @Override
    public void execute(Session client, String[] params) {
        String image = Comet.getServer().getConfig().get("comet.notification.avatar.prefix");

        if (params.length != 1) {
            EventPointsCommand.sendNotif("Oops! Voc\u00ea deve digitar apenas o nome do usu\u00e1rio que ir\u00e1 receber o ponto.", client);
            return;
        }

        String username = params[0];
        if (username.isEmpty()) {
            EventPointsCommand.sendNotif("Oops! O nome de usu\u00e1rio n\u00e3o pode estar em branco.", client);
            return;
        }

        if (username.contains(";")) {
            for (String nome : username.split("[;]")) {
                Session session = NetworkManager.getInstance().getSessions().getByPlayerUsername(nome);
                if (session != null) {
                    PlayerDao.updateEventPoints(nome);

                    String notif = "uma caixa suspresa no inventário";
                    int caixa = Integer.parseInt(NewConfig.get("gift.award.furniture"));
                    if(session.getPlayer().getData().isVip())
                    {
                        caixa = Integer.parseInt(NewConfig.get("gift.award.vip.furniture"));
                    }
                    long itemId = ItemDao.createItem(session.getPlayer().getId(), caixa, "");
                    final PlayerItem playerItem = new InventoryItem(itemId, caixa, "");
                    session.getPlayer().getInventory().addItem(playerItem);
                    session.send(new UpdateInventoryMessageComposer());
                    session.send(new UnseenItemsMessageComposer(Sets.newHashSet(playerItem)));


                    EventPointsCommand.sendNotif("Voc\u00ea recebeu " + notif, (Session)session);
                } else {
                    PlayerData playerData = PlayerDao.getDataByUsername(nome);
                    if (playerData != null) {
                        PlayerDao.updateEventPoints(nome);
                        int caixa = Integer.parseInt(NewConfig.get("gift.award.furniture"));
                        if(playerData.isVip())
                        {
                            caixa = Integer.parseInt(NewConfig.get("gift.award.vip.furniture"));
                        }

                        ItemDao.createItem(playerData.getId(), caixa, "");
                        playerData.save();
                    }
                }
            }

            String motd = "Os usu\u00e1rios ";

            for (String nome : username.split("[;]")) {
                motd = motd + nome + ", ";
            }

            EventPointsCommand.sendNotif(String.valueOf(motd) + "foram pagos.", client);

            if (CometSettings.enableEventWinnerNotification) {
                NetworkManager.getInstance().getSessions().broadcast(new NotificationMessageComposer("diamonds", String.valueOf(motd) + "ganharam o evento. Parabéns!"));
            }
        } else {
            Session session = NetworkManager.getInstance().getSessions().getByPlayerUsername(username);
            if (session != null) {
                PlayerDao.updateEventPoints(username);

                int caixa = Integer.parseInt(NewConfig.get("gift.award.furniture"));
                if(session.getPlayer().getData().isVip())
                {
                    caixa = Integer.parseInt(NewConfig.get("gift.award.vip.furniture"));
                }
                long itemId = ItemDao.createItem(session.getPlayer().getId(), caixa, "");
                final PlayerItem playerItem = new InventoryItem(itemId, caixa, "");
                session.getPlayer().getInventory().addItem(playerItem);
                session.send(new UpdateInventoryMessageComposer());
                session.send(new UnseenItemsMessageComposer(Sets.newHashSet(playerItem)));

                String notif = "uma caixa surpresa no inventário";


                EventPointsCommand.sendNotif("Voc\u00ea recebeu " + notif, session);

                String motd = "O usu\u00e1rio " + username;

                EventPointsCommand.sendNotif(String.valueOf(motd) + " recebeu " + notif, client);

                if (CometSettings.enableEventWinnerNotification) {
                    NetworkManager.getInstance().getSessions().broadcast(new NotificationMessageComposer(image.replace("{0}", session.getPlayer().getData().getUsername()), String.valueOf(motd) + " ganhou o evento. " + (username.equals("trylix") ? "Fa\u00e7a sexo com ele! e.e" : "Parab\u00e9ns!")));
                }

                return;
            }
            PlayerData playerData = PlayerDao.getDataByUsername(username);
            if (playerData != null) {
                PlayerDao.updateEventPoints(username);
                int caixa = Integer.parseInt(NewConfig.get("gift.award.furniture"));
                if(playerData.isVip())
                {
                    caixa = Integer.parseInt(NewConfig.get("gift.award.vip.furniture"));
                }
                ItemDao.createItem(playerData.getId(), caixa, "");
                playerData.save();
                return;
            }
        }

        EventPointsCommand.sendNotif("Oops! Ocorreu um erro e n\u00e3o foi poss\u00edvel entregar o ponto ao usu\u00e1rio.", client);
    }

    @Override
    public String getPermission() {
        return "eventpoint_command";
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.eventpoint.description");
    }
}
