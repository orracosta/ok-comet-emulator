package com.cometproject.server.game.rooms.objects.items.types.floor.freeze;

import com.cometproject.server.game.rooms.objects.entities.RoomEntity;
import com.cometproject.server.game.rooms.objects.entities.pathfinding.AffectedTile;
import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.game.rooms.types.components.games.GameTeam;
import com.cometproject.server.game.rooms.types.components.games.freeze.FreezeGame;
import com.cometproject.server.tasks.CometThreadManager;
import com.cometproject.server.tasks.executors.freeze.FreezeTileThrowBallEvent;

public class FreezeTileFloorItem extends RoomItemFloor {
    public FreezeTileFloorItem(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);
    }

    @Override
    public boolean onInteract(RoomEntity entity, int requestData, boolean isWiredTrigger) {
        if (entity == null || !(entity instanceof PlayerEntity) || isWiredTrigger) {
            return false;
        }

        if (this.getRoom().getGame().getInstance() == null || !(this.getRoom().getGame().getInstance() instanceof FreezeGame)) {
            return false;
        }

        if (((PlayerEntity)entity).getGameTeam() == null || ((PlayerEntity)entity).getGameTeam() == GameTeam.NONE || !((PlayerEntity)entity).getPlayer().getFreeze().canThrowBall()) {
            return false;
        }

        if (!this.getExtraData().equals("0") && !this.getExtraData().isEmpty()) {
            return false;
        }

        if (AffectedTile.tilesAdjecent(entity.getPosition().copy(), this.getPosition().copy())) {
            CometThreadManager.getInstance().executeOnce(new FreezeTileThrowBallEvent(this, (PlayerEntity)entity));
        }

        return true;
    }
}
