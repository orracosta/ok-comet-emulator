package com.cometproject.server.game.rooms.objects.items.types.floor.wired.actions;

import com.cometproject.server.game.rooms.objects.entities.RoomEntity;
import com.cometproject.server.game.rooms.objects.entities.effects.PlayerEffect;
import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.objects.items.types.floor.banzai.BanzaiGateFloorItem;
import com.cometproject.server.game.rooms.objects.items.types.floor.freeze.FreezeGateFloorItem;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.base.WiredActionItem;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.events.WiredItemEvent;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.game.rooms.types.components.games.GamePlayer;
import com.cometproject.server.game.rooms.types.components.games.GameTeam;

import java.util.List;


public class WiredActionJoinTeam extends WiredActionItem {
    private static final int PARAM_TEAM_ID = 0;

    /**
     * The default constructor
     *
     * @param id       The ID of the item
     * @param itemId   The ID of the item definition
     * @param room     The instance of the room
     * @param owner    The ID of the owner
     * @param x        The position of the item on the X axis
     * @param y        The position of the item on the Y axis
     * @param z        The position of the item on the z axis
     * @param rotation The orientation of the item
     * @param data     The JSON object associated with this item
     */
    public WiredActionJoinTeam(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);

        if (this.getWiredData().getParams().size() != 1) {
            this.getWiredData().getParams().put(PARAM_TEAM_ID, 1);
        }
    }

    @Override
    public boolean requiresPlayer() {
        return true;
    }

    @Override
    public int getInterface() {
        return 9;
    }
    @Override
    public void onEventComplete(WiredItemEvent event) {
        if (event.entity == null || !(event.entity instanceof PlayerEntity)) {
            return;
        }

        PlayerEntity playerEntity = (PlayerEntity) event.entity;
        if (playerEntity.getGameTeam() != GameTeam.NONE && playerEntity.getGameTeam() == this.getTeam()) {
            return;
        }

        playerEntity.setGameTeam(this.getTeam());
        getRoom().getGame().getTeams().get(this.getTeam()).add(new GamePlayer(playerEntity.getPlayerId()));

        playerEntity.applyEffect(new PlayerEffect(this.getTeam().getFreezeEffect(), false));

        List<FreezeGateFloorItem> gameGates = this.getRoom().getItems().getByClass(FreezeGateFloorItem.class);
        if (gameGates != null && gameGates.size() > 0) {
            for (RoomItemFloor floorItem : gameGates) {
                if (floorItem == null) {
                    continue;
                }

                if (!((BanzaiGateFloorItem) floorItem).getTeam().equals(this.getTeam())) {
                    continue;
                }

                floorItem.setExtraData(new StringBuilder().append(this.getRoom().getGame().getTeams().get(this.getTeam()).size()).toString());
                floorItem.sendUpdate();
            }
        }
    }

    private GameTeam getTeam() {
        switch (this.getWiredData().getParams().get(PARAM_TEAM_ID)) {

            case 1:
                return GameTeam.RED;
            case 2:
                return GameTeam.GREEN;
            case 3:
                return GameTeam.BLUE;
            case 4:
                return GameTeam.YELLOW;
        }

        return GameTeam.NONE;
    }
}
