package com.cometproject.server.game.rooms.objects.items.types.floor.wired.actions;

import com.cometproject.server.game.rooms.objects.entities.effects.PlayerEffect;
import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFactory;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.base.WiredActionItem;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.events.WiredItemEvent;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.network.messages.outgoing.room.avatar.WhisperMessageComposer;
import com.cometproject.server.game.rooms.objects.entities.RoomEntity;
import com.cometproject.server.game.rooms.objects.entities.RoomEntityType;


public class WiredActionFreezeAll extends WiredActionItem {

    protected boolean isWhisperBubble = false;
    public static final int PARAM_FREEZE_TIME = 0;

    /**
     * The default constructor
     *
     * @param id       The ID of the item
     * @param itemId   The ID of the item definition
     * @param room     The instance of the room
     * @param owner    The ID of the owner
     * @param x        The position of the item on the X axis
     * @param y        The position of the item on the Y axis
     * @param z        The position of the item on the z axis
     * @param rotation The orientation of the item
     * @param data     The JSON object associated with this item
     */
    public WiredActionFreezeAll(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);

        if (this.getWiredData().getParams().size() < 1) {
            this.getWiredData().getParams().clear();
            this.getWiredData().getParams().put(PARAM_FREEZE_TIME, 0);

        }
    }

    @Override
    public boolean requiresPlayer() {
        return false;
    }

    @Override
    public int getInterface() {
        return 20;
    }

    @Override
    public void onEventComplete(WiredItemEvent event) {
        if (event.entity == null) {
            //return;
        }

        PlayerEntity playerEntity = ((PlayerEntity) event.entity);


        if(this.getWiredData() == null || this.getWiredData().getText() == null) {
            return;
        }

        int time = this.getWiredData().getParams().get(PARAM_FREEZE_TIME);

        this.setTicks(RoomItemFactory.getProcessTime(time));

        for (RoomEntity entity : this.getRoom().getEntities().getPlayerEntities()) {
            if (entity.getEntityType() == RoomEntityType.PLAYER) {
                PlayerEntity pEntity = (PlayerEntity) entity;

                if(pEntity != playerEntity)
                {
                    if(this.getWiredData().getText().length() > 0)
                    {
                        pEntity.getPlayer().getSession().send(new WhisperMessageComposer(pEntity.getId(), this.getWiredData().getText().replaceAll("%username%", playerEntity.getUsername()), isWhisperBubble ? 0 : 34));
                    }
                    pEntity.cancelWalk();
                    pEntity.getPlayer().getEntity().setCanWalk(false);
                    pEntity.applyEffect(new PlayerEffect(12, time*2));
                }
            }
        }

    }

    @Override
    public void onTickComplete() {
        for (RoomEntity entity : this.getRoom().getEntities().getPlayerEntities()) {
            if (entity.getEntityType() == RoomEntityType.PLAYER) {
                PlayerEntity pEntity = (PlayerEntity) entity;

                    pEntity.getPlayer().getEntity().setCanWalk(true);
                    //pEntity.getPlayer().getSession().send(new WhisperMessageComposer(pEntity.getId(), "Você foi descongelado!", isWhisperBubble ? 0 : 34));
            }
            }
        }
    }
