package com.cometproject.server.game.rooms.objects.items.types.floor.freeze;

import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.types.Room;

public class FreezeExitTileFloorItem extends RoomItemFloor {
    public FreezeExitTileFloorItem(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);
    }
}
