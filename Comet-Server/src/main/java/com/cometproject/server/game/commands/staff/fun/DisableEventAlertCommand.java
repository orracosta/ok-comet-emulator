package com.cometproject.server.game.commands.staff.fun;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.network.sessions.Session;

public class DisableEventAlertCommand extends ChatCommand {
    @Override
    public void execute(Session client, String[] params) {
        if (client == null || client.getPlayer() == null || client.getPlayer().getSettings() == null) {
            return;
        }

        client.getPlayer().getSettings().setEnableEventNotif(false);

        DisableEventAlertCommand.sendNotif(Locale.get("command.disableeventnotif.txt"), client);
    }

    @Override
    public String getPermission() {
        return "disableeventnotif_command";
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.disableeventnotif.description");
    }
}
