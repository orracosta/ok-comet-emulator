package com.cometproject.server.game.commands.user;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.network.sessions.Session;


public class VipCommand extends ChatCommand {

    @Override
    public void execute(Session client, String message[]) {
       if(!client.getPlayer().getData().isVip())
       {
           client.getPlayer().sendAdvancedMessage(Locale.get("vip.promo.title"), Locale.get("vip.promo.text"), Locale.get("vip.promo.button"), Locale.get("vip.promo.link"), "vip_info");
       }
       else
       {
           int time = (int)System.currentTimeMillis()/1000;
           int diasr = (client.getPlayer().getData().getVipExpire() - time)/86400;
           client.getPlayer().sendNotif("Detalhes do clube", "<b><i>*Você tem "+diasr+" dias de VIP restantes</b></i>");
       }
    }

    @Override
    public String getPermission() {
        return "vip_command";
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.vip.description");
    }
}