package com.cometproject.server.game.commands.staff.rewards;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.game.players.data.PlayerData;
import com.cometproject.server.network.NetworkManager;
import com.cometproject.server.network.messages.outgoing.notification.AdvancedAlertMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.permissions.FuserightsMessageComposer;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.player.PlayerDao;


public class GiveVIPCommand extends ChatCommand {
    @Override
    public void execute(Session client, String[] params) {
        if (params.length < 2)
            return;

        String username = params[0];

        try {
            int credits = Integer.parseInt(params[1]);
            Session player = NetworkManager.getInstance().getSessions().getByPlayerUsername(username);

            if (player == null) {
                PlayerData playerData = PlayerDao.getDataByUsername(username);

                if (playerData == null) return;

                //playerData.increaseCredits(credits);
                if(!playerData.isVip()) {
                    int time = (int)System.currentTimeMillis()/1000;
                    int dias = credits+1;
                    int expire = (dias*86400)+ time;
                    playerData.setVipExpire(expire);
                    playerData.setVip(true);
                    playerData.saveNow();
                }
                else
                {
                    int time = playerData.getVipExpire();
                    int dias = credits+1;
                    int expire = (dias*86400)+ time;
                    playerData.setVipExpire(expire);
                    playerData.setVip(true);
                    playerData.saveNow();
                }
                //playerData.save();
                return;
            }

            if(!player.getPlayer().getData().isVip()) {
                int time = (int)System.currentTimeMillis()/1000;
                int dias = credits+1;
                int expire = (dias*86400)+ time;
                player.getPlayer().getData().setVipExpire(expire);
                player.getPlayer().getData().setVip(true);
                player.getPlayer().getData().saveNow();
                if(player.getPlayer().getData().getRank() < 4) {
                    player.getPlayer().getSession().sendQueue(new FuserightsMessageComposer(player.getPlayer().getSubscription().exists(), 4));
                }
                player.getPlayer().sendAdvancedMessage("Bem vindo ao clube!", "Você acabou de se juntar por "+(dias-1)+" dias ao clube mais badalado do hotel!<br><br>Nossas mais felizes BOAS VINDAS "+player.getPlayer().getData().getUsername()+"<br><br>Agora você possui todas as vantagens exclusivas de um membro VIP!<br><br>Aproveite bem o clube, você pode obter informações sobre sua assinatura com o comando <b>:vip</b>", "OK", "event:", "vip_info");
            }
            else
            {
                int time = player.getPlayer().getData().getVipExpire();
                int dias = credits+1;
                int expire = (dias*86400)+ time;
                player.getPlayer().getData().setVipExpire(expire);
                player.getPlayer().getData().setVip(true);
                player.getPlayer().getData().saveNow();
                player.getPlayer().sendAdvancedMessage("Assinatura extendida!", "Você acabou extender sua assinatura por "+(dias-1)+" dias ao clube mais badalado do hotel!<br><br>Você tem muita sorte "+player.getPlayer().getData().getUsername()+"<br><br>Agora você possui todas as vantagens exclusivas de um membro VIP por mais tempo!<br><br>Aproveite bem o clube, você pode obter informações sobre sua assinatura com o comando <b>:vip</b>", "OK", "event:", "vip_info");
            }
        } catch (Exception e) {
            client.send(new AdvancedAlertMessageComposer(Locale.get("command.coins.errortitle"), Locale.get("command.coins.formaterror")));
        }
    }

    @Override
    public String getPermission() {
        return "givevip_command";
    }

    @Override
    public String getParameter() {
        return Locale.getOrDefault("command.parameter.username" + " " + "command.parameter.amount", "%username% %amount%");
    }

    @Override
    public String getDescription() {
        return Locale.get("command.givevip.description");
    }
}
