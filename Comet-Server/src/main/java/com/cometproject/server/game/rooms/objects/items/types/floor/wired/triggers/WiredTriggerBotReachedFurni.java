package com.cometproject.server.game.rooms.objects.items.types.floor.wired.triggers;

import com.cometproject.server.game.rooms.objects.entities.types.BotEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.base.WiredTriggerItem;
import com.cometproject.server.game.rooms.types.Room;
import com.google.common.collect.Lists;

import java.util.Iterator;
import java.util.List;

public class WiredTriggerBotReachedFurni extends WiredTriggerItem {
    /**
     * The default constructor
     *
     * @param id        The ID of the item
     * @param itemId    The ID of the item definition
     * @param room      The instance of the room
     * @param owner     The ID of the owner
     * @param ownerName The username of the owner
     * @param x         The position of the item on the X axis
     * @param y         The position of the item on the Y axis
     * @param z         The position of the item on the z axis
     * @param rotation  The orientation of the item
     * @param data      The JSON object associated with this item
     */
    public WiredTriggerBotReachedFurni(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);
    }

    @Override
    public int getInterface() {
        return 13;
    }

    @Override
    public boolean suppliesPlayer() {
        return false;
    }

    public static boolean executeTriggers(BotEntity botEntity, RoomItemFloor floorItem) {
        boolean wasExecuted = false;

        for (RoomItemFloor wiredItem : getTriggers(botEntity.getRoom(), WiredTriggerBotReachedFurni.class)) {
            WiredTriggerBotReachedFurni trigger = ((WiredTriggerBotReachedFurni) wiredItem);

            if (trigger.getWiredData().getText().isEmpty())
                continue;

            String botName = trigger.getWiredData().getText();
            if (botEntity == null || !botEntity.getUsername().equals(botName) || floorItem == null)
                continue;

            wasExecuted = trigger.evaluate(botEntity, floorItem);
        }

        return wasExecuted;
    }
}
