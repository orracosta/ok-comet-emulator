package com.cometproject.server.game.commands.staff.banning;

import com.cometproject.server.boot.Comet;
import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.game.moderation.BanManager;
import com.cometproject.server.game.moderation.types.BanType;
import com.cometproject.server.network.NetworkManager;
import com.cometproject.server.network.sessions.Session;
import org.apache.commons.lang3.StringUtils;


public class BanCommand extends ChatCommand {
    @Override
    public void execute(Session client, String[] params) {
        if (params.length < 2) {
            sendNotif(Locale.getOrDefault("command.params.length", "Oops! You did something wrong!"), client);
            return;
        }

        String username = params[0];
        int length;
        if(StringUtils.isNumeric(params[1]) && params[1].length() > 8)
            length = 99999999;
        else
            length = StringUtils.isNumeric(params[1]) ? Integer.parseInt(params[1]) : 0;

        Session user = NetworkManager.getInstance().getSessions().getByPlayerUsername(username);

        if (user == null) {
            sendNotif(Locale.getOrDefault("command.user.offline", "This user is offline!"), client);
            return;
        }

        if (user == client || !user.getPlayer().getPermissions().getRank().bannable()) {
            sendNotif(Locale.getOrDefault("command.user.notbannable", "You're not able to ban this user!"), client);
            return;
        }

        String userId = Integer.toString(user.getPlayer().getId());

        if (BanManager.getInstance().hasBan(userId, BanType.USER)) {
            sendNotif("O usuário " + username + "já está banido!", client);
            return;
        }

        client.getPlayer().getStats().setBans(client.getPlayer().getStats().getBans() + 1);
        user.disconnect("banned");

        if(length > 9999999)
            length = 9999999;

        long expire = Comet.getTime() + (length * 3600);

        BanManager.getInstance().banPlayer(BanType.USER, user.getPlayer().getId() + "", length, expire, params.length > 2 ? this.merge(params, 2) : "", client.getPlayer().getId());
    }

    @Override
    public String getPermission() {
        return "ban_command";
    }
    
    @Override
    public String getParameter() {
        return Locale.getOrDefault("command.parameter.ban", "%username% %time% %reason%");
    }

    @Override
    public String getDescription() {
        return Locale.get("command.ban.description");
    }

    @Override
    public boolean bypassFilter() {
        return true;
    }
}
