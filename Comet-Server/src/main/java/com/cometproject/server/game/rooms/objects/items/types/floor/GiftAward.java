package com.cometproject.server.game.rooms.objects.items.types.floor;

import com.cometproject.api.game.players.data.components.inventory.PlayerItem;
import com.cometproject.server.config.CometSettings;
import com.cometproject.server.config.Locale;
import com.cometproject.server.game.players.components.types.inventory.InventoryItem;
import com.cometproject.server.game.rooms.objects.entities.RoomEntity;
import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.config.NewConfig;
import com.cometproject.server.network.messages.outgoing.catalog.UnseenItemsMessageComposer;
import com.cometproject.server.network.messages.outgoing.notification.NotificationMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.inventory.UpdateInventoryMessageComposer;
import com.cometproject.server.network.messages.outgoing.user.permissions.FuserightsMessageComposer;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.storage.queries.items.ItemDao;
import com.google.common.collect.Sets;

import java.util.Random;


public class GiftAward extends RoomItemFloor {
    public GiftAward(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);
    }

    private void Sorteio(PlayerEntity player)
    {
        String msg = "";
        int diamonds = CometSettings.eventWinnerRewardQuantity;
        Random rand = new Random();
        int chance = 0;
        if(player.getPlayer().getData().isVip())
        {
            chance = rand.nextInt(100);
            if(chance >= 0 && chance < 50)
            {
                // dima extra
                int dimas = rand.nextInt(Integer.parseInt(NewConfig.get("gift.award.vip.diamonds.max"))-Integer.parseInt(NewConfig.get("gift.award.vip.diamonds.min"))) + Integer.parseInt(NewConfig.get("gift.award.vip.diamonds.min"));
                diamonds += dimas;
                msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra de "+dimas+" diamantes";
                player.getPlayer().getData().increasePoints(diamonds);
                player.getPlayer().getData().save();
                player.getPlayer().sendBalance();
            }
            else if(chance >= 50 && chance < 85)
            {
                // emblema
                String emblemas[] = NewConfig.get("gift.award.vip.badges").split(";");
                int i = rand.nextInt(emblemas.length-1);
                String emblema = emblemas[i];
                if(player.getPlayer().getInventory().hasBadge(emblema))
                {
                    Sorteio(player);
                }
                else
                {
                    player.getPlayer().getInventory().addBadge(emblema, true, true);
                    msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra: novo emblema";
                    player.getPlayer().getData().increasePoints(diamonds);
                    player.getPlayer().getData().save();
                    player.getPlayer().sendBalance();
                }
            }
            else if(chance >= 85 && chance < 95)
            {
                // raro
                String raros[] = NewConfig.get("gift.award.vip.items").split(";");
                int i = rand.nextInt(raros.length-1);
                String raro = raros[i];
                Session client = player.getPlayer().getSession();
                msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra: mobi raro";
                long itemId = ItemDao.createItem(client.getPlayer().getId(), Integer.parseInt(raro), "");
                final PlayerItem playerItem = new InventoryItem(itemId, Integer.parseInt(raro), "");
                client.getPlayer().getInventory().addItem(playerItem);
                client.send(new UpdateInventoryMessageComposer());
                client.send(new UnseenItemsMessageComposer(Sets.newHashSet(playerItem)));
            }
            else
            {
                // dias vip
                int dias = rand.nextInt(5) + 6;
                msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra de "+dias+" dias de VIP";
                int expire = (dias*86400)+ player.getPlayer().getData().getVipExpire();
                player.getPlayer().getData().setVipExpire(expire);
                player.getPlayer().getData().saveNow();
            }
        }
        else
        {
            chance = rand.nextInt(100);
            if(chance >= 0 && chance < 70)
            {
                msg = "Você recebeu "+CometSettings.eventWinnerRewardQuantity+" diamantes";
                player.getPlayer().getData().increasePoints(diamonds);
                player.getPlayer().getData().save();
                player.getPlayer().sendBalance();
            }
            else if(chance >= 70 && chance < 80)
            {
                // dima extra
                int dimas = rand.nextInt(Integer.parseInt(NewConfig.get("gift.award.diamonds.max"))-Integer.parseInt(NewConfig.get("gift.award.diamonds.min"))) + Integer.parseInt(NewConfig.get("gift.award.diamonds.min"));
                diamonds += dimas;
                msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra de "+dimas+" diamantes";
                player.getPlayer().getData().increasePoints(diamonds);
                player.getPlayer().getData().save();
                player.getPlayer().sendBalance();
            }
            else if(chance >= 80 && chance < 90)
            {
                // emblema
                String emblemas[] = NewConfig.get("gift.award.badges").split(";");
                int i = rand.nextInt(emblemas.length-1);
                String emblema = emblemas[i];
                if(player.getPlayer().getInventory().hasBadge(emblema))
                {
                    Sorteio(player);
                }
                else
                {
                    player.getPlayer().getInventory().addBadge(emblema, true, true);
                    msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra: novo emblema";
                    player.getPlayer().getData().increasePoints(diamonds);
                    player.getPlayer().getData().save();
                    player.getPlayer().sendBalance();
                }
            }
            else if(chance >= 90 && chance < 95)
            {
                // raro
                String raros[] = NewConfig.get("gift.award.items").split(";");
                int i = rand.nextInt(raros.length-1);
                String raro = raros[i];
                Session client = player.getPlayer().getSession();
                msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra: mobi raro";
                long itemId = ItemDao.createItem(client.getPlayer().getId(), Integer.parseInt(raro), "");
                final PlayerItem playerItem = new InventoryItem(itemId, Integer.parseInt(raro), "");
                client.getPlayer().getInventory().addItem(playerItem);
                client.send(new UpdateInventoryMessageComposer());
                client.send(new UnseenItemsMessageComposer(Sets.newHashSet(playerItem)));
            }
            else
            {
                // dias
                int dias = 3;
                //msg = "Você recebeu "+diamonds+" diamantes<br><b>*Prêmio extra de "+dias+" dias de VIP";
                int time = (int)System.currentTimeMillis()/1000;
                int expire = (dias*86400)+ time;
                player.getPlayer().getInventory().addBadge("VIP", true);
                player.getPlayer().getData().setVipExpire(expire);
                player.getPlayer().getData().setVip(true);
                player.getPlayer().getData().saveNow();
                if(player.getPlayer().getData().getRank() < 4)
                {
                    player.getPlayer().getSession().sendQueue(new FuserightsMessageComposer(player.getPlayer().getSubscription().exists(), 4));
                }
                player.getPlayer().sendAdvancedMessage("Bem vindo ao clube!", "Você acabou de se juntar por 3 dias ao clube mais badalado do hotel!<br><br>Nossas mais felizes BOAS VINDAS "+player.getPlayer().getData().getUsername()+"<br><br>Agora você possui todas as vantagens exclusivas de um membro VIP!<br><br>Aproveite bem o clube, você pode obter informações sobre sua assinatura com o comando <b>:vip</b>", "OK", "event:", "vip_info");
            }
        }
        if(msg.length()>1) {
            player.getPlayer().sendNotif(Locale.get("gift.award.title"), msg);
        }
    }

    @Override
    public boolean onInteract(RoomEntity entity0, int requestData, boolean isWiredTrigger) {
        if (!isWiredTrigger) {
            if (!(entity0 instanceof PlayerEntity)) {
                return false;
            }

            PlayerEntity pEntity = (PlayerEntity) entity0;

            if (pEntity.getPlayer().getId() != this.ownerId) {
                pEntity.getPlayer().sendAdvancedMessage(Locale.get("gift.award.title"), Locale.get("gift.award.text"), "OK", "event:", "gift_award");
                return false;
            }

            // função de abrir

            Sorteio(pEntity);

            // remove a box
            this.getRoom().getItems().removeItem(this, null, false, true);
            return true;
        }
        return false;
    }

    @Override
    public boolean isMovementCancelled(RoomEntity entity) {
        return !this.isOpen();
    }

    public boolean isOpen() {
        return !this.getExtraData().equals("0");
    }
}
