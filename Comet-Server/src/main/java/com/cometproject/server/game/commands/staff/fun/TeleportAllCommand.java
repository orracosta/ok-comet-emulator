package com.cometproject.server.game.commands.staff.fun;

import com.cometproject.server.config.Locale;
import com.cometproject.server.game.commands.ChatCommand;
import com.cometproject.server.game.rooms.objects.misc.Position;
import com.cometproject.server.network.sessions.Session;
import com.cometproject.server.game.rooms.objects.entities.RoomEntityType;
import com.cometproject.server.game.rooms.objects.entities.RoomEntity;
import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.entities.effects.PlayerEffect;

public class TeleportAllCommand extends ChatCommand {
    @Override
    public void execute(Session client, String[] params) {
        if (client == null || client.getPlayer() == null || client.getPlayer().getSettings() == null) {
            return;
        }

        Position squareInFront = client.getPlayer().getEntity().getPosition().squareInFront(client.getPlayer().getEntity().getBodyRotation());

        for (RoomEntity entity : client.getPlayer().getEntity().getRoom().getEntities().getPlayerEntities()) {
            if (entity.getEntityType() == RoomEntityType.PLAYER) {
                PlayerEntity pEntity = (PlayerEntity) entity;

                if(client.getPlayer().getEntity() != pEntity) {
                    pEntity.applyEffect(new PlayerEffect(4, 5));
                    pEntity.cancelWalk();
                    pEntity.setCanWalk(false);
                    pEntity.warp(squareInFront);
                }
            }
        }

    }

    @Override
    public String getPermission() {
        return "tpa_command";
    }

    @Override
    public String getParameter() {
        return "";
    }

    @Override
    public String getDescription() {
        return Locale.get("command.tpa.description");
    }
}
