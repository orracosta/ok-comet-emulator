package com.cometproject.server.game.rooms.objects.items.types.floor.wired.actions;

import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.base.WiredActionItem;
import com.cometproject.server.game.rooms.objects.items.types.floor.wired.events.WiredItemEvent;
import com.cometproject.server.game.rooms.types.Room;
import com.cometproject.server.network.messages.outgoing.room.avatar.WhisperMessageComposer;

public class WiredActionMuteTriggerer extends WiredActionItem {
    public static final int PARAM_MUTE_TIME = 0;

    /**
     * The default constructor
     *
     * @param id       The ID of the item
     * @param itemId   The ID of the item definition
     * @param room     The instance of the room
     * @param owner    The ID of the owner
     * @param x        The position of the item on the X axis
     * @param y        The position of the item on the Y axis
     * @param z        The position of the item on the z axis
     * @param rotation The orientation of the item
     * @param data     The JSON object associated with this item
     */
    public WiredActionMuteTriggerer(long id, int itemId, Room room, int owner, String ownerName, int x, int y, double z, int rotation, String data) {
        super(id, itemId, room, owner, ownerName, x, y, z, rotation, data);

        if (this.getWiredData().getParams().size() < 1) {
            this.getWiredData().getParams().clear();
            this.getWiredData().getParams().put(PARAM_MUTE_TIME, 0);
        }
    }

    @Override
    public boolean requiresPlayer() {
        return true;
    }

    @Override
    public int getInterface() {
        return 20;
    }

    @Override
    public void onEventComplete(WiredItemEvent event) {
        if (event.entity == null || !(event.entity instanceof PlayerEntity)) {
            return;
        }

        int time = this.getWiredData().getParams().get(PARAM_MUTE_TIME);

        String message = this.getWiredData().getText();

        if (time > 0) {
            ((PlayerEntity)event.entity).getPlayer().getSession().send(new WhisperMessageComposer(((PlayerEntity)event.entity).getPlayerId(), "Wired Mute: Silenciado por " + time + (time > 1 ? "minutos" : "minuto") + "! Message: " + (message == null || message.isEmpty() ? "Nenhuma mensagem" : message)));

            if (this.getRoom().getRights().hasMute(((PlayerEntity)event.entity).getPlayerId())) {
                this.getRoom().getRights().updateMute(((PlayerEntity)event.entity).getPlayerId(), time);
            } else {
                this.getRoom().getRights().addMute(((PlayerEntity)event.entity).getPlayerId(), time);
            }
        }
    }
}