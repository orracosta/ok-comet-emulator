package com.cometproject.server.tasks.executors.freeze;

import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.tasks.CometTask;

import java.util.List;

public class FreezeResetTileEvent implements CometTask {
    private final List<RoomItemFloor> floorItems;

    public FreezeResetTileEvent(List<RoomItemFloor> floorItems) {
        this.floorItems = floorItems;
    }

    @Override
    public void run() {
        for (RoomItemFloor floorItem : this.floorItems) {
            floorItem.setExtraData("0");
            floorItem.sendUpdate();
        }
    }
}
