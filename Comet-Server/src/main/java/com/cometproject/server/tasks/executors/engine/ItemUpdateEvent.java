package com.cometproject.server.tasks.executors.engine;

import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.tasks.CometTask;

public class ItemUpdateEvent implements CometTask {
    private final RoomItemFloor floorItem;
    private final String state;

    public ItemUpdateEvent(RoomItemFloor floorItem, String state) {
        this.floorItem = floorItem;
        this.state = state;
    }

    @Override
    public void run() {
        if (this.floorItem == null || this.state.isEmpty()) {
            return;
        }

        this.floorItem.setExtraData(this.state);
        this.floorItem.sendUpdate();
    }
}
