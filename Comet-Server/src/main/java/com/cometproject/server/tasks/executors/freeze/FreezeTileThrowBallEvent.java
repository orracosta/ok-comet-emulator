package com.cometproject.server.tasks.executors.freeze;

import com.cometproject.server.game.rooms.objects.entities.types.PlayerEntity;
import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.tasks.CometTask;
import com.cometproject.server.tasks.CometThreadManager;
import com.cometproject.server.tasks.executors.engine.ItemUpdateEvent;

import java.util.concurrent.TimeUnit;

public class FreezeTileThrowBallEvent implements CometTask {
    private final RoomItemFloor floorItem;
    private final PlayerEntity entity;
    private final int radius;

    public FreezeTileThrowBallEvent(RoomItemFloor floorItem, PlayerEntity entity) {
        this.floorItem = floorItem;
        this.entity = entity;
        this.radius = entity.getPlayer().getFreeze().getBoost();
    }

    @Override
    public void run() {
        this.entity.getPlayer().getFreeze().decreaseBall();

        this.floorItem.setExtraData("" + this.radius * 1000);
        this.floorItem.sendUpdate();

        CometThreadManager.getInstance().executeSchedule(new FreezeTileBallExplosionEvent(this.floorItem, this.entity, this.radius), 2000, TimeUnit.MILLISECONDS);
        CometThreadManager.getInstance().executeSchedule(new ItemUpdateEvent(this.floorItem, "11000"), 2000, TimeUnit.MILLISECONDS);
        CometThreadManager.getInstance().executeSchedule(new ItemUpdateEvent(this.floorItem, "0"), 3000, TimeUnit.MILLISECONDS);
    }
}
