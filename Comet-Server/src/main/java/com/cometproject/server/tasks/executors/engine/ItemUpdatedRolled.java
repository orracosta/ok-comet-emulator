package com.cometproject.server.tasks.executors.engine;

import com.cometproject.server.game.rooms.objects.items.RoomItemFloor;
import com.cometproject.server.tasks.CometTask;

public class ItemUpdatedRolled implements CometTask {
    private final RoomItemFloor floorItem;
    private final Boolean isMoving;

    public ItemUpdatedRolled(RoomItemFloor floorItem, boolean isMoving) {
        this.floorItem = floorItem;
        this.isMoving = isMoving;
    }

    @Override
    public void run() {
        if (this.floorItem == null) {
            return;
        }

        this.floorItem.setIsMoving(this.isMoving);
    }
}
